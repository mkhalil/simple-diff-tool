namespace Sdt
{
	[GtkTemplate (ui = "/org/gnome/sdt/ui/sdt-application-window.ui")]
	public class ApplicationWindow : Gtk.ApplicationWindow
	{
		[GtkChild]
		private Gtk.HeaderBar d_header_bar;

		[GtkChild]
		private Gtk.Stack d_main_stack;

		private MainView d_main_view;
		private DiffView d_diff_view;

		private Ggit.Repository repo;
		
		private Ggit.DiffLineCallback line_callback;
		private Ggit.DiffHunkCallback hunk_callback;

		public ApplicationWindow(Sdt.Application app)
		{
			Object(application : app);
		}

		construct
		{
			Ggit.init();
			build_ui();
		}

		private void build_ui()
		{
			d_main_view = new MainView();
			d_diff_view = new DiffView();

			d_main_stack.add_named(d_diff_view, "diff_view");
			d_main_stack.add_named(d_main_view, "main_view");

			d_main_stack.set_visible_child_name("main_view");

			d_main_view.add_project_button.clicked.connect(() => {
				var repo_url = d_main_view.add_project_entry.text;
				add_project_repo(repo_url);
			});

			d_diff_view.compare_button.clicked.connect(() => {
				d_diff_view.diff_text_view.buffer.text = "";
				compare_commits();
			});
		}

		private void add_project_repo(string url)
		{
			try
			{
				var location = File.new_for_path(url);
				repo = Ggit.Repository.open(location);
				
				d_main_stack.set_visible_child_name("diff_view");
				d_header_bar.title = repo.workdir.get_basename();
			}
			catch(Error err)
			{
				print(err.message + "\n");
			}
		}

		private void compare_commits()
		{
			var first_commit_sha = d_diff_view.first_commit_sha_entry.text;
			var second_commit_sha = d_diff_view.second_commit_sha_entry.text;

			var first_commit_OId = new Ggit.OId.from_string(first_commit_sha);
			var second_commit_OId = new Ggit.OId.from_string(second_commit_sha);

			try
			{
				var first_commit = repo.lookup_commit(first_commit_OId);
				var second_commit = repo.lookup_commit(second_commit_OId);

				var diff = new Ggit.Diff.tree_to_tree(repo, first_commit.get_tree(), second_commit.get_tree(), null);

				hunk_callback = write_hunk;
				line_callback = write_line;
				
				diff.foreach(
					(delta, progress) => {
						return 0;
					},
					(delta, binary) => {
						return 0;
					},
					hunk_callback,
					line_callback);
			}
			catch(Error err)
			{
				print(err.message + "\n");
			}
		}
		
		public int write_line(Ggit.DiffDelta delta, Ggit.DiffHunk? hunk, Ggit.DiffLine line)
		{
			if(line.get_origin() == Ggit.DiffLineType.ADDITION)
				d_diff_view.diff_text_view.buffer.text += "+" + " " + line.get_new_lineno().to_string() + " ";
			else if(line.get_origin() == Ggit.DiffLineType.DELETION)
				d_diff_view.diff_text_view.buffer.text += line.get_old_lineno().to_string() + " " + "- ";
			else
				d_diff_view.diff_text_view.buffer.text += line.get_old_lineno().to_string() + " " + line.get_new_lineno().to_string() + " ";
				
			d_diff_view.diff_text_view.buffer.text += line.get_text();
			return 0;
		}
		
		public int write_hunk(Ggit.DiffDelta delta, Ggit.DiffHunk hunk)
		{
			d_diff_view.diff_text_view.buffer.text += hunk.get_header();
			return 0;
		}
	}
}
